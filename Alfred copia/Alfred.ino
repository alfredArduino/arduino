//Keypad attached to pins 2, 3, 4, 5, 6, 7
//Alarma attached to A2
//gas A4
//Ultrason suma attached to 9,10
//Ultrason resta attached to 11,12
//------------------------------------

#include <Key.h>
#include <Keypad.h>

//Declaration pins:
int buzzer = A2;
const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char keys[ROWS][COLS] =
{ '1', '2', '3',
  '4', '5', '6',
  '7', '8', '9',
  '*', '0', '#'
};

byte rowPins[ROWS] = {2, 3, 4, 5}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {6, 7, 8}; //connect to the column pinouts of the keypad
Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS );

int trigX = 9;
int echoX = 10;
int trigY = 11;
int echoY = 12;

//Declaration variables:
char pass[4] = {'0', '0', '0', '0'}; // our secret (!) number
char var[4] = {'*', '*', '*', '*'}; //per reescriure
char var2[4] = {'*', '*', '*', '*'};
char changeSec[4] = {'#', '#', '#', '#'}; // per canviar la pass
int i = 0; //4 cifras contraseña
int j = 0; //numero de intentos
int gas = 0; //Lectura gas
int contador = 0;


//-------------------------------------------------------------------------------------------------------
// ---- Custom Functions ---- //
//-------------------------------------------------------------------------------------------------------

//***************************ENTRADA************************************

char *readInput() {

  char key = keypad.getKey();
  if (key != NO_KEY && i < 4) {
    var[i] = key;
    i++;
    tone(buzzer, 880);
    delay(100);
    noTone(buzzer);
    Serial.println(var);
    if (i == 4) {
      i = 0;
      return var;
    }
  }
  return NULL;
}

char *readSecondInput() {

  char key = keypad.getKey();
  if (key != NO_KEY && i < 4) {
    var2[i] = key;
    i++;
    tone(buzzer, 880);
    delay(100);
    noTone(buzzer);
    Serial.println(var);
    if (i == 4) {
      i = 0;
      return var2;
    }
  }
  return NULL;
}

bool checkStrings(char str1[4], char str2[4]) {
  if (str1[0] == str2[0] && str1[1] == str2[1] && str1[2] == str2[2] && str1[3] == str2[3]) {
    return true;
  } else {
    return false;
  }
}

void changePassword() {
  noTone(buzzer);//1
  tone(buzzer, 1760);
  delay(100);
  noTone(buzzer);//2
  delay(75);
  tone(buzzer, 1760);
  delay(100);
  noTone(buzzer);//3
  delay(75);
  tone(buzzer, 1760);
  delay(150);
  noTone(buzzer);

  int comp = 0;
  while ( comp == 0) {
    Serial.println("Canvi");
    char *input = readInput();
    if (checkStrings(input, pass)) {
      //Serial.println("Correct");
      tone(buzzer, 1760);
      delay(200);
      noTone(buzzer);
      while ( comp == 0) {
        digitalWrite(12, LOW);
        Serial.println("primer");
        char *pass1 = readInput();
        while (pass1 != NULL) {
          //posar so buzzer
          char *pass2 = readSecondInput();
          //          Serial.print("Primer:");
          //          Serial.println(pass1);
          //          Serial.print("Segon:");
          //          Serial.println(pass2);
          if (checkStrings(pass1, pass2)) {
            // Serial.println("Canviando...");
            pass[0] = pass1[0]; pass[1] = pass1[1]; pass[2] = pass1[2]; pass[3] = pass1[3];
            comp = 1;
            pass1 = NULL;
            tone(buzzer, 1760);
            delay(200);
            noTone(buzzer);
          }
          else if (pass2 != NULL) {
             Serial.println("Torna a introduir");
            pass1 = NULL;
            tone(buzzer, 146);
            delay(200);
            noTone(buzzer);
          }
        }
      }
    }
    else if (input != NULL) {
      // Serial.println("Incorrecte");
      tone(buzzer, 1760);
      delay(200);
      noTone(buzzer);
      break;
    }
  }
}

void entrada() {
  char *sec = readInput();

  if (checkStrings(sec, changeSec)) {
    changePassword();

  } else if (checkStrings(sec, pass)) {
    // usuari correcte
     Serial.println("Correct");
    tone(buzzer, 1760);
    delay(200);
    noTone(buzzer);

  } else if (sec != NULL) {
    // Serial.println("Incorrecte");
    tone(buzzer, 146);
    delay(400);
    noTone(buzzer);
    j++;
    if (j == 3) {
      //fer sonar buzzer
       Serial.println("Bring the alarm");
      for (int z = 0; z < 10; z++) {
        tone(buzzer, 440);
        delay(500);
        noTone(buzzer);
      }
      j = 0;
    }
  }
}
//***************************ENTRADA************************************


//***************************GAS************************************
void gasAlarm() {
  // determine alarm status
  if (gas >= 600)
  {
    for (int y = 0; y < 10; y++) {
      tone(buzzer, 440);
      delay(500);
      noTone(buzzer);
    }
  }
}
//***************************GAS************************************

//***************************ULTRASONIDO************************************
void ultrasonido() {
  long duration, distance;
  digitalWrite(trigX, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigX, HIGH);
  delayMicroseconds(10); // Added this line
  digitalWrite(trigX, LOW);
  duration = pulseIn(echoX, HIGH);
  Serial.print("duration");
  Serial.println(duration);
  distance = (duration / 2) / 29.1;
  Serial.print("Distance: ");
  Serial.println(distance);

  if (distance < 7) {
    contador++;
    Serial.print(contador);
    delay(800);
  }

  long duration2, distance2;
  digitalWrite(trigY, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigY, HIGH);
  delayMicroseconds(10); // Added this line
  digitalWrite(trigY, LOW);
  duration2 = pulseIn(echoY, HIGH);
  distance2 = (duration2 / 2) / 29.1;
    Serial.print("Distance 2: ");
    Serial.println(distance2);

  if (distance2 < 7) {
    contador--;
    if(contador<0){
      contador=0;
    }
    Serial.print(contador);
    delay(800);
  }
}
//***************************ULTRASONIDO************************************


//***********************LEER SENSORES*********************************
void readSensors() {

  gas = analogRead(A4);
    Serial.print("gas=");
    Serial.println(gas);
  delay(10);

}
//***********************LEER SENSORES*********************************


//-------------------------------------------------------------------------------------------------------
//--------SETUP-------//
//-------------------------------------------------------------------------------------------------------
void setup() {

  Serial.begin(9600);
  pinMode(buzzer, OUTPUT);
  
  pinMode(trigX, OUTPUT);
  pinMode(echoX, INPUT);
  pinMode(trigY, OUTPUT);
  pinMode(echoY, INPUT);

}

//-------------------------------------------------------------------------------------------------------
// -------LOOP---------//
//-------------------------------------------------------------------------------------------------------
void loop() {

  entrada();
  ultrasonido();
  readSensors();
  gasAlarm();

}
