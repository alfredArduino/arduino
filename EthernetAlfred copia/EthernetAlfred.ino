//pin libre:  A4, A5 6,8,9

//Led comedor attached to 3
//Led calefaccion 2
//Led aire 5
//Servo attached to pin 7
//Sensor de luz attached to A1
//DHT11 attached to A3
//Ethernet shield attached to pins 4, 10, 11, 12, 13


//-------------------------------------------------
#include <SPI.h>
#include <Ethernet.h>
#include <dht.h>
#include <Servo.h>


// Enter a MAC address and IP address for your controller below.
byte mac[] = {0x90, 0xA2, 0xDA, 0x0D, 0x48, 0xD3 };

// assign an IP address for the controller dependent on your local network:

IPAddress ip(192, 168, 1, 99);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

// Initialize the Ethernet server library with the port you want to use.
EthernetServer server(80);
String readString;

// Declaration pins:
int ledComedor = 3; //pin attach
int buzzer = A2;
dht DHT;
#define DHT11_PIN A3
Servo myservo;



// Declaration variables:
int lightSensor = 0; //lectura sensor luz
int valueLuz = 0; //valor que se recibe de internet, variable por el usuario sobre la LUZ
int valueTemperatura = 0;
int luzON = 0; //encender o apagar luces
int ClimatizationON = 0; //encender apagar temperatura
int temperatura = 0; //lectura temperatura
int humedad = 0;//lectura humedad
int Alarma = 0;


//*********************ENCENDER LUCES*******************************
void encenderLuces() {

  if (luzON == 1) {

    Serial.println("LuzOn");
    int  intensidad = valueLuz - lightSensor; //calculamos la intensidad con el valor que ha introducido el usuario y la luz disponible
    if (intensidad < 0) {
      intensidad = 0;
    }
    Serial.print("valorluz: ");
    Serial.println(valueLuz);
    Serial.print("intensidad");
    Serial.println(intensidad);
    analogWrite(ledComedor, intensidad);
  }


  if (luzON == 0) {
    analogWrite(ledComedor, 0);
  }

}
//*********************ENCENDER LUCES*******************************

//*********************ENCENDER TEMPERATURA*******************************
void encenderTemperatura() {
  Serial.println(ClimatizationON);
  delay(500);
  Serial.println(valueTemperatura);
  delay(500);

  if (ClimatizationON == 1) {
    if (valueTemperatura < temperatura) {
      digitalWrite(5, HIGH);
      digitalWrite(2, LOW);
      Serial.println("Led groc");
      delay(500);
    }
    if (valueTemperatura > temperatura) {
      digitalWrite(2, HIGH);
      digitalWrite(5, LOW);
      Serial.println("Led verd");
      delay(500);
    }
    else {
      digitalWrite(2, LOW); //aire
      digitalWrite(5, LOW); //calefaccion
    }
  }
  if (ClimatizationON == 0) {
    digitalWrite(2, LOW); //aire
    digitalWrite(5, LOW); //calefaccion
  }
}
//*********************ENCENDER TEMPERATURA*******************************

//***********************LEER SERIAL*********************************
void leerSerial() {
  while (Serial.available() > 0) {
    if (Alarma == 1) {
      for (int y = 0; y < 10; y++) {
        tone(buzzer, 440);
        delay(500);
        noTone(buzzer);
      }
    }
    if (Alarma == 0) {
      noTone(buzzer);
    }

    char inChar = (char) Serial.read();

    if (inChar == '0') {
      luzON = 0;
    }
    else {
      luzON = 1;
    }
  }
}
//***********************LEER SERIAL*********************************

//***********************LEER SENSORES*********************************
void readSensors() {

  lightSensor = analogRead(A1)/4; // analogRead values go from 0 to 1023, analogWrite values from 0 to 255
  Serial.print("Sensor luz cris: ");
  Serial.println(lightSensor);
  int chek = DHT.read11(DHT11_PIN);
  humedad = DHT.humidity;
  temperatura = DHT.temperature;
  Serial.print(humedad);
  Serial.print(",\t");
  Serial.println(temperatura);

}
//***********************LEER SENSORES*********************************

//*******************PARAMETROS DE INTERNET****************************
void getParamsComedor(EthernetClient client) {
  int firstSpaceIndex = readString.indexOf(" ");
  int secondSpaceIndex = readString.indexOf(" ", firstSpaceIndex + 1);
  String firstVal = readString.substring(0, firstSpaceIndex);
  String secondVal = readString.substring(firstSpaceIndex + 1, secondSpaceIndex);

  if (readString.indexOf("intensityComedor") > 0) {
    int indexValue = secondVal.indexOf("=");
    String key = "intensity";
    String iString = secondVal.substring(indexValue + 1, secondVal.length());
    valueLuz = iString.toInt();

  }

  if (readString.indexOf("temperatureComedor") > 0) {
    int indexValue = secondVal.indexOf("=");
    String key = "temperatura";
    String iString = secondVal.substring(indexValue + 1, secondVal.length());
    valueTemperatura = iString.toInt();
  }

}
//*******************PARAMETROS DE INTERNET****************************

//**************************COMEDOR************************************
void implementationComedor(EthernetClient client) {
  //Serial.println("implementation diningroom: " + readString);
  getParamsComedor(client); //mirar valors

  if (readString.indexOf("?lightComedorOn") > 0) //checks for on
  {
    luzON = 1;
  }

  if (readString.indexOf("?lightComedorOff") > 0) //checks for off
  {
    luzON = 0;
  }

  if (readString.indexOf("?persianasComedorUp") > 0) //checks for Up
  {
    myservo.write(0);
    delay(3000);
    myservo.write(90);
    Serial.print("Up");
    delay(500);
  }

  if (readString.indexOf("?persianasComedorDown") > 0) //checks for Down
  {
    myservo.write(180);
    delay(3000);
    myservo.write(90);
    Serial.print("Down");
    delay(500);
  }

  if (readString.indexOf("?climatizacionComedorOn") > 0) //checks for climatization On
  {
    ClimatizationON = 1;
  }

  if (readString.indexOf("?climatizacionComedorOff") > 0) //checks for climatization Off
  {
    ClimatizationON = 0;
  }

  if (readString.indexOf("?AlarmaOn") > 0) //checks for climatization Off
  {
    ClimatizationON = 1;
  }

  if (readString.indexOf("?AlarmaOff") > 0) //checks for climatization Off
  {
    ClimatizationON = 0;
  }
}
//**************************COMEDOR************************************



//-------------------------------------------------------------------------------------------------------
//--------SETUP-------//
//-------------------------------------------------------------------------------------------------------
void setup()
{
  pinMode(2, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(buzzer, OUTPUT);
  myservo.attach(7);
  myservo.write(90);


  Serial.begin(9600);

  //start Ethernet
  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());

}

//-------------------------------------------------------------------------------------------------------
// -------LOOP---------//
//-------------------------------------------------------------------------------------------------------

void loop()
{
  readSensors();//llegir sensors
  encenderLuces();
  encenderTemperatura();
  leerSerial();


  // listen for incoming clients
  EthernetClient client = server.available();

  if (client)
  {
    // Serial.println("client: " + client);

    while (client.connected())
      //  Serial.println("Client connected"); //debug
    {
      if (client.available())
        //  Serial.println("Client available"); //debug
      {
        char c = client.read();

        //read char by char HTTP request
        if (readString.length() < 100)
        {
          //store characters to string
          readString += c;
          //Serial.print(c);

          Serial.write(c);
          Serial.println(readString);
          // if you've gotten to the end of the line (received a newline
          // character) and the line is blank, the http request has ended,
          // so you can send a reply
          //if HTTP request has ended
          if (c == '\n') {
            Serial.println(readString); //print to serial monitor for debuging
            //------------------------------------------------------------------------------

            // what is being Displayed :

            client.println("<TITLE>Home Automation</TITLE>");
            client.println("<center>");
            client.println("</HEAD>");
            client.println("<BODY>");

            implementationComedor(client);//el que ha de fer el programa

            client.println("</BODY>");

            //-----------------------------------------------------------------------------------------------------------

            delay(1);
            //stopping client
            client.stop();

            //clearing string for next read
            readString = "";

            // give the web browser time to receive the data
            delay(1);
            // close the connection:
            client.stop();
            Serial.println("client disonnected");

          }
        }
      }
    }
  }
}



